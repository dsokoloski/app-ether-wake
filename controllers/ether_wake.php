<?php

/**
 * Ether Wake controller.
 *
 * @category   Apps
 * @package    Ether_Wake
 * @subpackage Controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    GPLv3
 */

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Ether Wake controller.
 *
 * @category   Apps
 * @package    Ether_Wake
 * @subpackage Controllers
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    GPLv3
 */

class Ether_wake extends ClearOS_Controller
{
    /**
     * Ether Wake default controller.
     *
     * @return view
     */

    function index()
    {
        // Load dependencies
        //------------------

        $this->lang->load('ether_wake');

        // Load views
        //-----------

        $views = array('ether_wake/device');
	$this->page->view_forms($views, lang('ether_wake_app_name'));
    }
}
