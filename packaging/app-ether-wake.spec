
Name: app-ether-wake
Epoch: 1
Version: 1.0.0
Release: 1%{dist}
Summary: Ether Wake
License: GPLv3
Group: ClearOS/Apps
Packager: Darryl Sokoloski <dsokoloski@clearfoundation.com>
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base

%description
Ether Wake is a tool to send a Wake-On-LAN "Magic Packet" to configured network devices, interactively or from a set schedule.

%package core
Summary: Ether Wake - Core
License: LGPLv3
Group: ClearOS/Libraries
Requires: app-base-core
Requires: net-tools
Requires: app-tasks-core
Requires: app-network-core >= 1:1.4.38

%description core
Ether Wake is a tool to send a Wake-On-LAN "Magic Packet" to configured network devices, interactively or from a set schedule.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/ether_wake
cp -r * %{buildroot}/usr/clearos/apps/ether_wake/

install -D -m 0644 packaging/ether_wake.conf %{buildroot}/etc/clearos/ether_wake.conf

%post
logger -p local6.notice -t installer 'app-ether-wake - installing'

%post core
logger -p local6.notice -t installer 'app-ether-wake-core - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/ether_wake/deploy/install ] && /usr/clearos/apps/ether_wake/deploy/install
fi

[ -x /usr/clearos/apps/ether_wake/deploy/upgrade ] && /usr/clearos/apps/ether_wake/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ether-wake - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ether-wake-core - uninstalling'
    [ -x /usr/clearos/apps/ether_wake/deploy/uninstall ] && /usr/clearos/apps/ether_wake/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/ether_wake/controllers
/usr/clearos/apps/ether_wake/htdocs
/usr/clearos/apps/ether_wake/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/ether_wake/packaging
%exclude /usr/clearos/apps/ether_wake/tests
%dir /usr/clearos/apps/ether_wake
/usr/clearos/apps/ether_wake/deploy
/usr/clearos/apps/ether_wake/language
/usr/clearos/apps/ether_wake/libraries
%config(noreplace) /etc/clearos/ether_wake.conf
