<?php

$lang['ether_wake_app_description'] = 'Ether Wake is a tool to send a Wake-On-LAN "Magic Packet" to configured network devices, interactively or from a set schedule.';
$lang['ether_wake_app_name'] = 'Ether Wake';
$lang['ether_wake_broadcast'] = 'Broadcast?';
$lang['ether_wake_device_add'] = 'Add Device';
$lang['ether_wake_device_already_exists'] = 'Device already exists';
$lang['ether_wake_device_edit'] = 'Edit Device';
$lang['ether_wake_device_ident'] = 'Identifier';
$lang['ether_wake_device_not_found'] = 'Device not found';
$lang['ether_wake_devices'] = 'Devices';
$lang['ether_wake_device_wake_all'] = 'Wake All';
$lang['ether_wake_device_wake'] = 'Wake';
$lang['ether_wake_help_ident'] = 'Station (MAC) address or a host-ID that can be translated to a MAC address by an ethers database (usually /etc/ethers).'; 
$lang['ether_wake_help_password'] = 'A six byte password may be specified in Ethernet hex format (00:22:44:66:88:aa) or four byte dotted decimal (192.168.1.1) format.  A four byte password must use the dotted decimal format.';
$lang['ether_wake_help_schedule'] = 'If you would like to automatically wake up this device every day, specify an hour below.';
$lang['ether_wake_invalid_identifier'] = 'Invalid device identifier';
$lang['ether_wake_invalid_password'] = 'Invalid password';
$lang['ether_wake_schedule'] = 'Scheduled Wake-ups?';
$lang['ether_wake_send_invalid_interface'] = 'Invalid interface';
$lang['ether_wake_send_invalid_parameter'] = 'Invalid parameter';
$lang['ether_wake_send_permission_denied'] = 'Permission denied';
$lang['ether_wake_send_test_wake'] = 'Test Wake';
$lang['ether_wake_send_unknown_error'] = 'Unknown error';
$lang['ether_wake_subcategory_management_tools'] = 'Management Tools';
