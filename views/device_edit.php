<?php

/**
 * Ether Wake device configuration.
 * @category   ClearOS
 * @package    Ether Wake
 * @subpackage Views
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/date/
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('ether_wake');
$this->lang->load('network');

///////////////////////////////////////////////////////////////////////////////
// Form handler
///////////////////////////////////////////////////////////////////////////////

$buttons = array( 
    ($mode == 'add') ?
        form_submit_add('submit-form') :
        form_submit_update('submit-form'),
    anchor_cancel('/app/ether_wake')
);

///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////

$readonly = ($mode == 'edit') ? TRUE : FALSE;
$device_ident = ($mode == 'edit') ?
    ((strlen($device['hostname']) > 0) ? $device['hostname'] : $ident) :
    '';
$device_interface = ($mode == 'edit') ? $device['interface'] : '';
$device_broadcast = ($mode == 'edit') ? $device['broadcast'] : FALSE;
$device_password = ($mode == 'edit') ? $device['password'] : '';
$device_schedule = ($mode == 'edit') ?
    ((count($schedule) > 0) ? TRUE : FALSE) : FALSE;
$device_hour = ($mode == 'edit') ?
    ((count($schedule) > 0) ? $schedule[1] : 0) : 0;

$hours = array();
for ($hour = 0; $hour < 24; $hour++)
    $hours[$hour] = sprintf('%02d', $hour);

echo form_open(
    'ether_wake/device/' . $mode,
    array('id' => 'device_form')
);
echo form_header($title, array('id' => 'device'));

// Device identifier
echo form_banner(lang('ether_wake_help_ident'));
echo field_input('identifier', $device_ident,
    lang('network_mac_address') . ' / ' . lang('network_hostname'),
    $readonly);
if ($readonly == FALSE && count($ethers) > 1) {
    echo field_simple_dropdown('ethers', $ethers,
        (array_key_exists($ident, $ethers)) ?
            $device_ident : '', '', FALSE);
}

// Device interface
echo field_simple_dropdown('interface', $interfaces, $device_interface,
    lang('network_interface'), FALSE);

// Device broadcast flag
echo field_checkbox('broadcast', $device_broadcast,
    lang('ether_wake_broadcast'), FALSE);

// Device password
echo form_banner(lang('ether_wake_help_password'));
echo field_input('password', $device_password,
    lang('base_password'), FALSE);

// Ether Wake schedule for device
echo form_banner(lang('ether_wake_help_schedule'));
echo field_checkbox('schedule', $device_schedule,
    lang('ether_wake_schedule'), FALSE);
echo field_simple_dropdown('hour', $hours, $device_hour,
    lang('base_hour'), FALSE);

echo field_button_set($buttons);

echo form_footer();
echo form_close();

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
