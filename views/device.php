<?php

/**
 * Ether Wake controller.
 *
 * @category   Apps
 * @package    Ether_Wake
 * @subpackage Views
 * @author     Darryl Sokoloski <dsokoloski@clearfoundation.com>
 * @copyright  2013 ClearFoundation
 * @license    GPLv3
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('ether_wake');
$this->lang->load('network');
$this->lang->load('base');

///////////////////////////////////////////////////////////////////////////////
// Headers
///////////////////////////////////////////////////////////////////////////////

$headers = array(
    lang('ether_wake_device_ident'),
    lang('network_interface'),
    lang('ether_wake_broadcast'),
);

///////////////////////////////////////////////////////////////////////////////
// Items
///////////////////////////////////////////////////////////////////////////////

$items = array();

foreach ($devices as $ident => $device) {
    $id = preg_replace('/:/', '_', $ident);
    $broadcast = ($device['broadcast']) ?
        lang('base_yes') : lang('base_no');

    $item['title'] = $ident;
    $item['action'] = '';
    $item['anchors'] = button_set(array(
        anchor_edit('/app/ether_wake/device/edit/' .
            urlencode($ident)),
        anchor_delete('/app/ether_wake/device/delete/' .
            urlencode($ident)),
        anchor_custom('/app/ether_wake/device/wake/' .
            urlencode($ident), lang('ether_wake_device_wake')),
    ));
    $item['details'] = array(
        (strlen($device['hostname'])) ? $device['hostname'] : $ident,
        "<span id='interface_$id'>{$device['interface']}</span>",
        "<span id='broadcast_$id'>$broadcast</span>",
    );

    $items[] = $item;
}

///////////////////////////////////////////////////////////////////////////////
// Summary table
///////////////////////////////////////////////////////////////////////////////

echo summary_table(
    lang('ether_wake_devices'),
    array(
        anchor_custom('/app/ether_wake/device/add',
            lang('ether_wake_device_add')),
        anchor_custom('/app/ether_wake/device/wake_all',
            lang('ether_wake_device_wake_all')),
    ),
    $headers,
    $items,
    array('id' => 'ether_wake_device_summary')
);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
